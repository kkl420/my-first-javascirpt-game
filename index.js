"use strict";

class Renderer {
    constructor(element) {
        this.element = element
        this.setup();
    }
    setup() {
        let box = document.createElement("div");
        box.style.position = "absolute";
        box.style.top = "20px";
        box.style.left = "50px";
        box.style.backgroundColor = "red";
        box.style.width = "20px";
        box.style.height = "20px";
        this.element.appendChild(box);
        this.box = box;
    }
    render(position) {
        this.box.style.top = position + "px";
    }
}

class Box {
    constructor() {
        this.position = 0;
        this.speed = 0;
    }

    changePosition() {
        this.speed++
        this.position = this.position+this.speed;
    }
    moveUp(){
        this.speed = -20;
    }
}

class Game {
    constructor(element){
        this.renderer = new Renderer(element);
        this.box = new Box()
        this.element = element;
        this.isRunning = true;
        this.setup();
    }
    setup() {
        this.element.addEventListener("click",()=>{
            this.box.moveUp();
        },false);
    }

    start() {
        let points = 0;
        let id = setInterval(()=>{
            points++;
            this.box.changePosition();
            if(this.box.position < 0) {
                console.log("Game Over")
                this.isRunning = false;
                clearInterval(id);
                this.addPointstoHighscore(points);
            }
            if (this.box.position + 20 > this.element.clientHeight) {
                console.log("Game Over");
                this.isRunning = false;
                clearInterval(id);
                this.addPointstoHighscore(points);
            }
            if (this.isRunning == true){
                this.renderer.render(this.box.position);
            }
            
        },50);
    }
    addPointstoHighscore(points){
        let table = document.getElementById("highScore");
        let row = document.createElement("tr");
        row.innerHTML = points;

        table.append(row);

    }
}

let game = new Game(document.getElementById("game"));
game.start();



